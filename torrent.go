package gorrent

import (
	"bitbucket.org/sadasant/gorrent/messages"
	"bytes"
	bencode "code.google.com/p/bencode-go"
	"crypto/sha1"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type Torrent struct {
	Announce string // URL of the tracker
	Info     TorrentInfo
}

// In localData we'll store the pieces according to the hash...
// TODO:
// This approach is wrong, it means we need to store a whole copy of the files in RAM.
// Generate a temp file with the data, or read the data directly from the local files.
var localData = map[string][]byte{}

type TorrentInfo struct {
	// PieceLength is the number of bytes in each piece
	PieceLength int
	// Pieces is a string consisting of the concatenation of all
	// 20-byte SHA1 hash values, one per piece (byte string, i.e. not
	// urlencoded).
	Pieces string

	// FOR SINGLE FILE MODE
	// Name is the filename. It's purely advisory.
	Name string
	// Length is the Length of the file in bytes
	Length int

	// FOR MULTIPLE-FILE MODE
	Files []TorrentFile
}

type TorrentFile struct {
	// Length of the file in bytes
	Length int
	// Path is a list containing one or more string elements that together
	// represent the path and filename. Each element in the list
	// corresponds to either a directory name or (in the case of the final
	// element) the filename. For example, a the file "dir1/dir2/file.ext"
	// would consist of three string elements: "dir1", "dir2", and
	// "file.ext". This is encoded as a bencoded list of strings such as
	// l4:dir14:dir28:file.exte
	Path []string
}

type TorrentSession struct {
	Torrent    *Torrent
	TR         *TrackerResponse
	Downloaded int
	Uploaded   int
	Left       int
	Requests   int
	Data       []byte
	Pieces     []*Piece
	Connects   map[string]*Connection
	Index      int
}

func NewTorrent(path string) (torrent *Torrent, err error) {
	torrent = new(Torrent)
	if strings.HasPrefix(path, "http:") {
		// Location of the torrent to download.
		panic("Not implemented")
	} else if strings.HasPrefix(path, "magnet:") {
		// Magnet link torrent
		panic("Not implemented")
	} else if strings.HasSuffix(path, ".torrent") {
		// Torrent file
		panic("Not implemented")
	} else {
		info, err := os.Stat(path)
		if err != nil {
			return nil, err
		}
		if info.IsDir() {
			log.Printf("[TORRENT] Adding directory: %s", path)
			err = torrent.addDirectory(path)
		} else {
			log.Printf("[TORRENT] Adding file: %s", path)
			err = torrent.addFile(path)
		}
	}
	log.Printf("NEW TORRENT: %#v", torrent)
	return
}

// Adding a file to torrent.Info.Pieces
func (torrent *Torrent) addFile(path string) (err error) {
	info, err := os.Stat(path)
	if err != nil {
		return
	}

	file, err := os.Open(path)
	if err != nil {
		return
	}

	// Reading the file bytes
	size := info.Size()
	b := make([]byte, size)
	_, err = file.Read(b)
	if err != nil {
		return
	}

	// The piece length specifies the nominal piece size, and is usually a power of 2
	// Current best-practice is to keep the piece size to 512KB or less
	b_length := len(b)
	pieces, piece_length := calculatePieces(b_length)
	torrent.Info.PieceLength = piece_length

	for i := 0; i < pieces; i++ {
		sha1_hash := sha1.New()
		start := i * torrent.Info.PieceLength
		end := start + torrent.Info.PieceLength
		if end > b_length {
			end = b_length
		}
		piece := b[start:end]
		sha1_hash.Write(piece)
		hash := string(sha1_hash.Sum(nil))
		torrent.Info.Pieces += hash
		localData[hash] = piece
	}

	// Adding other info
	torrent.Info.Name = info.Name()

	// FIXME: Int conversion might cause troubles for big files...?
	torrent.Info.Length = int(size)
	return nil
}

func (torrent *Torrent) addDirectory(path string) error {
	info, err := os.Stat(path)
	if err != nil {
		return err
	}
	if !info.IsDir() {
		return errors.New("Path is not a directory")
	}
	// TODO: What if a specific path name was given?
	abs, err := filepath.Abs(path)
	if err != nil {
		return err
	}
	dir := filepath.Base(abs)
	torrent.Info.Name = dir

	// For the purposes of piece boundaries in the multi-file case,
	// consider the file data as one long continuous stream, composed
	// of the concatenation of each file in the order listed in the
	// files list. The number of pieces and their boundaries are then
	// determined in the same manner as the case of a single file.
	//
	// Pieces may overlap file boundaries.
	var data []byte
	filepath.Walk(abs, func(path string, info os.FileInfo, err error) error {
		// TODO: Remove restrictions
		if path != abs && !strings.HasPrefix(path, abs+"/.git") && !strings.HasSuffix(path, ".test") && !info.IsDir() && filepath.Ext(path) != ".test" {
			log.Printf("Adding: %q", path)
			b, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			data = append(data, b...)
			torrent.Info.Length += int(info.Size())
			torrent.Info.Files = append(torrent.Info.Files, TorrentFile{
				Length: int(info.Size()),
				Path:   strings.Split(strings.TrimPrefix(path, abs), "/")[1:],
			})
		}
		return nil
	})

	data_length := len(data)
	pieces, piece_length := calculatePieces(data_length)
	torrent.Info.PieceLength = piece_length

	for i := 0; i < pieces; i++ {
		sha1_hash := sha1.New()
		start := i * torrent.Info.PieceLength
		end := start + torrent.Info.PieceLength
		if end > data_length {
			end = data_length
		}
		piece := data[start:end]
		sha1_hash.Write(piece)
		hash := string(sha1_hash.Sum(nil))
		torrent.Info.Pieces += hash
		localData[hash] = piece
	}

	return nil
}

func (torrent *Torrent) NewSession(local bool, index int) *TorrentSession {
	TS := new(TorrentSession)
	TS.Index = index
	TS.Torrent = torrent
	TS.TR = &TrackerResponse{}

	info := torrent.Info

	if !local {
		TS.Left = info.Length
	}


	// ceil(total length / piece length)
	pieces := info.Length / info.PieceLength
	pieces++

	var length int
	for i := 0; i < pieces; i++ {
		hashIndex := i * 20
		// We need to fill the blocks according to the localData
		// TODO: Change this approach to read from a tmp file or from disk directly.
		hash := info.Pieces[hashIndex : hashIndex+20]
		var data []byte
		if local {
			data = localData[hash]
		}
		if i < pieces-1 {
			length = info.PieceLength
		} else {
			length = info.Length - (info.PieceLength * (pieces-1))
		}
		// println(length)
		piece := NewPiece(i, length, hash, data)
		TS.Pieces = append(TS.Pieces, piece)
	}

	TS.Data = make([]byte, info.Length)
	TS.Connects = map[string]*Connection{}
	log.Printf("[TORRENT] NewSession: {Left: %v, Pieces: %v, Completed: %v, Attended: %v}", TS.Left, len(TS.Pieces), len(TS.GetCompleted()), len(TS.GetAttended()))
	return TS
}

func (i TorrentInfo) Hash() (string, error) {
	var b bytes.Buffer
	if err := bencode.Marshal(&b, i); err != nil {
		return "", err
	}
	hash := sha1.New()
	hash.Write(b.Bytes())
	return string(hash.Sum(nil)), nil
}

func (TS *TorrentSession) GetCompleted() (pieces []*Piece) {
	for _, piece := range TS.Pieces {
		if piece.IsComplete() {
			pieces = append(pieces, piece)
		}
	}
	return
}

func (TS *TorrentSession) GetAttended() (pieces []*Piece) {
	for _, piece := range TS.Pieces {
		if piece.IsActive() {
			pieces = append(pieces, piece)
		}
	}
	return
}

func (TS *TorrentSession) GetUnattended() (pieces []*Piece) {
	for _, piece := range TS.Pieces {
		if !(piece.IsActive() || piece.IsComplete()) {
			pieces = append(pieces, piece)
		}
	}
	return
}

func (TS *TorrentSession) processPiece(m messages.Message) (err error) {
	pieceMsg, err := m.ToPiece()
	if err != nil {
		return
	}

	offset := int(pieceMsg.BlockOffset)

	piece := TS.Pieces[pieceMsg.PieceIndex]
	piece.SetBlock(offset, pieceMsg.BlockData)
	if !piece.IsComplete() {
		return
	}
	log.Printf("[TORRENT] Piece #%v IS COMPLETE", piece.Index)

	// Check if the hash is ok
	if !piece.ValidHash() {
		piece.Reset()
		return fmt.Errorf("Piece #%v IS CORRUPT", piece.Index)
	}

	// So the leecher now can seed this piece! :D
	begin := piece.Index * TS.Torrent.Info.PieceLength
	end := begin + len(piece.Data)
	if end > TS.Torrent.Info.Length {
		end = TS.Torrent.Info.Length
	}
	copy(TS.Data[begin:end], piece.Data)
	localData[piece.Hash] = piece.Data
	return
}

func calculatePieces(data_length int) (int, int) {
	var pieces int
	var _pieces int
	pL := 2
	_pL := pL
	for {
		_pL *= 2
		_pieces = data_length / _pL
		if _pieces < 2 || _pieces*_pL > data_length {
			break
		}
		pL = _pL
		pieces = data_length / pL
		// Avoid huge pieces
		if pL >= 512*1000 {
			break
		}
	}
	// Taking the pieces
	// The last piece might be very empty, who cares.
	pieces++
	return pieces, pL
}
