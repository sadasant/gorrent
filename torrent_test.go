package gorrent

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestSingleFile(t *testing.T) {
	// In this test, we'll create a torrent containing a file (this
	// README.mkd).  We'll assign that torrent to a seeder, it will
	// have it available so a leecher can download it and write it to
	// the `_results` directory.

	// INDEX:
	// 0) Remove the previously completed files
	// 1) Create the torrent
	// 2) Create a tracker
	// 3) Create a first client, the seeder.
	// 4) Create a second client, the leecher.
	// 5) Start tracking the torrent with both clients.
	// 6) Wait for the file to appear in the _results folder.
	// 7) Comparte the original with the downloaded file.

	// 0) Remove the previous completed files
	os.Remove("_results/README.mkd")

	// 1) Create the torrent
	// We'll manually define the torrent info,
	// Instead of providing the metainfo .torrent file.
	file_path := "README.mkd"
	torrent, err := NewTorrent(file_path)
	if err != nil {
		t.Fatalf("TORRENT FAILED: %s", err)
	}
	torrent.Announce = "http://127.0.0.1:8080/"

	// 2) Create a tracker
	tracker := NewTracker(8080, 1) // Port, Interval
	go tracker.Listen()

	// 3) Create a first client, the seeder.
	seeder := NewClient()
	seeder.Id = "SEEDER>>>>>>>>>>>>>>"
	seeder.AddLocalTorrent(torrent)
	go seeder.Listen()

	// 4) Create a second client, the leecher.
	leecher := NewClient()
	leecher.Id = "LEECHER<<<<<<<<<<<<<"
	leecher.Port = 6882
	leecher.DownloadPath = "_results"
	leecher.AddTorrent(torrent)
	go leecher.Listen()

	// 5) Start tracking the torrent with both clients.
	go seeder.TrackTorrent(0)
	go leecher.TrackTorrent(0)

	// 6) Wait for the file to appear in the _results folder.
	for {
		time.Sleep(5 * time.Second)
		_, err := os.Stat(leecher.DownloadPath + "/" + file_path)
		if os.IsNotExist(err) {
			continue
		}
		break
	}

	// 7) Comparte the original with the downloaded file.
	original, _ := ioutil.ReadFile(file_path)
	download, _ := ioutil.ReadFile(leecher.DownloadPath + "/" + file_path)
	if len(original) != len(download) || !bytes.EqualFold(original, download) {
		t.Fatalf("File contents doesn't match!")
	}

	// Bye bye!
	tracker.Stop()
	seeder.Stop()
	leecher.Stop()
}

func TestWholeDirectory(t *testing.T) {
	// In this test, we'll create a torrent containing the whole
	// directory of the repo. We'll assign the torrent to a seeder and a
	// leecher will download it, in a new directory inside the `_results`
	// directory.

	// INDEX:
	// 0) Remove the previously completed files
	// 1) Create the torrent
	// 2) Create a tracker
	// 3) Create a first client, the seeder.
	// 4) Create a second client, the leecher.
	// 5) Start tracking the torrent with both clients.
	// 6) Wait for the files to appear in the _results folder.
	// 7) Comparte the original files with the downloaded files.

	// 0) Remove the previous completed files
	os.Remove("_results/README.mkd")
	os.RemoveAll("_results/gorrent")

	// 1) Create the torrent
	// We'll manually define the torrent info,
	// Instead of providing the metainfo .torrent file.
	//
	// Not every file in the directory is included,
	// there are some conditions hardcoded in torrent.go:L173
	// just to simplify the tests.
	directory := "."
	torrent, err := NewTorrent(directory)
	if err != nil {
		t.Fatalf("TORRENT FAILED: %s", err)
	}
	torrent.Announce = "http://127.0.0.1:8080/"

	// 2) Create a tracker
	tracker := NewTracker(8080, 1) // Port, Interval
	go tracker.Listen()

	// 3) Create a first client, the seeder.
	seeder := NewClient()
	seeder.Id = "SEEDER>>>>>>>>>>>>>>"
	seeder.AddLocalTorrent(torrent)
	go seeder.Listen()

	// 4) Create a second client, the leecher.
	leecher := NewClient()
	leecher.Id = "LEECHER<<<<<<<<<<<<<"
	leecher.Port = 6882
	leecher.DownloadPath = "_results"
	leecher.AddTorrent(torrent)
	go leecher.Listen()

	// 5) Start tracking the torrent with both clients.
	go seeder.TrackTorrent(0)
	go leecher.TrackTorrent(0)

	// 6) Wait for the files to appear in the _results folder.
	for {
		time.Sleep(5 * time.Second)
		_, err := os.Stat(leecher.DownloadPath + "/gorrent/utils.go")
		if os.IsNotExist(err) {
			continue
		}
		break
	}

	// 7) Comparte the original files with the downloaded files.
	filepath.Walk("_results/gorrent", func(path string, info os.FileInfo, err error) error {
		original, _ := ioutil.ReadFile(info.Name())
		download, _ := ioutil.ReadFile(path)
		if len(original) != len(download) || !bytes.EqualFold(original, download) {
			t.Fatalf("File contents doesn't match! Original: %q, Download: %q", info.Name(), path)
		}
		return nil
	})

	// Bye bye!
	tracker.Stop()
	seeder.Stop()
	leecher.Stop()
}
