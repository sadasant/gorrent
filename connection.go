package gorrent

import (
	"bitbucket.org/sadasant/gorrent/messages"
	"bytes"
	"encoding/binary"
	"log"
	"net"
	"time"
)

type Connection struct {
	am_chocking     bool
	am_interested   bool
	peer_chocking   bool
	peer_interested bool
	Data            []byte
	Addr            net.TCPAddr
	Conn            net.Conn
	Client          *Client
	Peer            Peer
}

func NewConnection(client *Client, peer Peer) *Connection {
	c := new(Connection)
	c.Client = client
	c.Peer = peer
	return c
}

func (c *Connection) Connect() {
	c.Addr = net.TCPAddr{net.ParseIP(c.Peer.Ip), c.Peer.Port, ""}
	straddr := c.Addr.String()
	log.Printf("[%s] Connecting to %s: %s...", c.Client.Id, c.Peer.Id, straddr)
	var err error
	c.Conn, err = net.DialTimeout("tcp", straddr, time.Second*5)
	if err != nil {
		log.Printf("[%s] Unable to connect to %s: %s...", c.Client.Id, c.Peer.Id, straddr)
	} else {
		log.Printf("[%s] Connected to %s: %s...", c.Client.Id, c.Peer.Id, straddr)
	}
}

func (c *Connection) SendMessage(message interface{}) {
	err := binary.Write(c.Conn, binary.BigEndian, message)
	if err != nil {
		log.Printf("[%s] Couldn't send message: %s", c.Client.Id, err)
	}
}

func (c *Connection) SendHandshake(infoHash string) {
	c.SendMessage(messages.NewHandshake(infoHash, c.Client.Id))
}

func (c *Connection) SendRequest(pieceIndex, blockOffset, blockLength int) {
	c.SendMessage(messages.NewRequest(uint32(pieceIndex), uint32(blockOffset), uint32(blockLength)))
}

func (c *Connection) SendPiece(pieceIndex, blockOffset uint32, blockData []byte) {
	// The line below doesn't work because io.Write dislikes slices...
	// c.SendMessage(messages.NewPiece(pieceIndex, blockOffset, blockData))
	p := messages.NewPiece(pieceIndex, blockOffset, blockData)
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, p.Header)
	binary.Write(buf, binary.BigEndian, p.PieceIndex)
	binary.Write(buf, binary.BigEndian, p.BlockOffset)
	binary.Write(buf, binary.BigEndian, p.BlockData)
	byt := buf.Bytes()
	log.Printf("[%s] Sending Piece len: %v, bD len: %v, offset: %v", c.Client.Id, len(byt), len(blockData), p.BlockOffset)
	// log.Printf("[%s] Sending Piece: % x, bD len: %v", c.Client.Id, byt, len(blockData))
	_, err := c.Conn.Write(byt)
	if err != nil {
		log.Printf("[%s] Couldn't send message: %s", c.Client.Id, err)
	}
}

func (c *Connection) SendInterested() {
	if c.am_interested {
		log.Printf("[%s] Sending interested", c.Client.Id)
		c.SendMessage(messages.NewInterested())
	} else {
		log.Printf("[%s] Sending not interested", c.Client.Id)
		c.SendMessage(messages.NewNotInterested())
	}
}

func (c *Connection) Exchange(TS *TorrentSession) {
	if c.peer_chocking {
		return
	}

GETU:
	unattended := TS.GetUnattended()
	interested := len(unattended) > 0
	log.Printf("[%s] INTERESTED: %v, Unattended: %v", c.Client.Id, interested, unattended)
	if interested != c.am_interested {
		c.am_interested = interested
		c.SendInterested()
	}
	if !interested {
		return
	}

	// Choose a random piece that I don't have
	piece := randomPiece(unattended)

	// TODO: And what if no one answered?
	// TS.Requests++

	for {
		block := piece.NextBlock()
		if block == nil {
			log.Printf("[%s] Piece #%v: No NextBlock, data is full.", c.Client.Id, piece.Index)
			break
		}
		log.Printf("[%s] Requesting to peer %v, piece #%v, block: {Begin: %v, Length: %v}", c.Client.Id, c.Peer.Id, piece.Index, block.Begin, block.Length)
		c.SendRequest(piece.Index, block.Begin, block.Length)
	}
	goto GETU
}

func (c *Connection) processMessage(m messages.Message, TS *TorrentSession) {
	if m.Header.Length == 0 {
		log.Printf("[%s] Peer %v says: Keep alive", c.Client.Id, c.Peer.Id)
		return
	}
	switch m.Header.Id {
	case messages.ChokeId:
		log.Printf("[%s] Peer %v says: Chocked", c.Client.Id, c.Peer.Id)
		c.peer_chocking = true
	case messages.UnchokeId:
		c.peer_chocking = false
		log.Printf("[%s] Peer %v says: Unchocked", c.Client.Id, c.Peer.Id)
		// TODO:
		// pm.downloadPiece(c.Peer)
	case messages.InterestedId:
		log.Printf("[%s] Peer %v says: InterestedId", c.Client.Id, c.Peer.Id)
		// TODO: Unchoke?
		// Whether or not the remote peer is interested in something
		// this client has to offer. This is a notification that the
		// remote peer will begin requesting blocks when the client
		// unchokes them.
		//
		// A block is downloaded by the client when the client is
		// interested in a peer, and that peer is not choking the
		// client. A block is uploaded by a client when the client
		// is not choking a peer, and that peer is interested in the
		// client.
		c.peer_interested = true
	case messages.NotInterestedId:
		log.Printf("[%s] Peer %v says: NotInterestedId", c.Client.Id, c.Peer.Id)
		c.peer_interested = false
	case messages.HaveId:
		log.Printf("[%s] Peer %v says: HaveId", c.Client.Id, c.Peer.Id)
		// TODO:
		// c.Peer.SetHave(m)
		// pm.downloadPiece(c.Peer)
	case messages.BitFieldId:
		log.Printf("[%s] Peer %v says: BitFieldId", c.Client.Id, c.Peer.Id)
		// TODO:
		// c.Peer.SetBitField(m)
		// pm.downloadPiece(c.Peer)
	case messages.RequestId:
		if !c.peer_interested {
			return
		}
		req, err := m.ToRequest()
		if err != nil {
			log.Printf("[%s] ToRequest failed: %s", c.Client.Id, err)
			return
		}
		int_index := int(req.PieceIndex)
		if int_index > len(TS.Pieces) {
			log.Printf("[%s] ToRequest: PieceIndex out of range: %v > %v", c.Client.Id, int_index, len(TS.Pieces))
			return
		}
		piece := TS.Pieces[int_index]
		block_index := int(req.BlockOffset) / piece.BlockLength
		log.Printf("[%s] Peer %v says: RequestId, PieceIndex: %v, block_index: %v, offset: %v, Pieces: %v", c.Client.Id, c.Peer.Id, int_index, block_index, int(req.BlockOffset), len(TS.Pieces))
		if block_index >= len(piece.Blocks) {
			log.Printf("[%s] ToRequest: block_index out of range: %v > %v", c.Client.Id, block_index, len(piece.Blocks))
			return
		}
		block := piece.Blocks[block_index]
		c.SendPiece(req.PieceIndex, req.BlockOffset, block)
	case messages.PieceId:
		// TODO: The if below is not right, but interested/chocking stuff must work!
		// if !c.am_interested {
		// 	return
		// }
		log.Printf("[%s] Peer %v says: PieceId", c.Client.Id, c.Peer.Id)
		// TODO: Is this right?
		TS.Requests--
		err := TS.processPiece(m)
		if err != nil {
			log.Printf("[%s] processPiece ERROR: %s", c.Client.Id, err)
		}
		// If we're done...
		if len(TS.GetCompleted()) == len(TS.Pieces) {
			log.Printf("[TORRENT] ALL PIECES ARE COMPLETE")
			err := c.Client.writeFiles(TS.Index)
			if err != nil {
				log.Printf("[%s] writeFiles ERROR: %s", c.Client.Id, err)
			}
		}
	case messages.CancelId:
		log.Printf("[%s] Peer %v says: CancelId", c.Client.Id, c.Peer.Id)
	case messages.PortId:
		log.Printf("[%s] Peer %v says: PortId", c.Client.Id, c.Peer.Id)
	default:
		log.Printf("[%s] Peer %v says: UNKNOWN", c.Client.Id, c.Peer.Id)
	}
}
