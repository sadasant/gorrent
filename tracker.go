package gorrent

import (
	"bufio"
	"bytes"
	bencode "code.google.com/p/bencode-go"
	"log"
	"net"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Tracker struct {
	Port     int
	Interval int
	Torrents map[string]trackedTorrents
	listener net.Listener
	ch       chan bool
	wG       *sync.WaitGroup
}

type trackedTorrents struct {
	Complete   int
	Incomplete int
	Peers      []Peer
}

type TrackerResponse struct {
	FailureReason  string "failure reason"
	WarningMessage string "warning message"
	Interval       int
	MinInterval    int    "min interval"
	TrackerId      string "tracker id"
	Complete       int
	Incomplete     int
	Peers          []Peer
}

func NewTracker(port, interval int) *Tracker {
	tracker := new(Tracker)
	tracker.Port = port
	tracker.Interval = interval
	tracker.Torrents = map[string]trackedTorrents{}
	tracker.ch = make(chan bool)
	tracker.wG = &sync.WaitGroup{}
	return tracker
}

func (tracker *Tracker) Listen() {
	tracker.wG.Add(1)
	defer tracker.wG.Done()
	tcp, err := net.ResolveTCPAddr("tcp", ":"+strconv.Itoa(tracker.Port))
	if err != nil {
		log.Fatalf("[TRACKER] ResolveTCPAddr error: %v", err)
		return
	}
	ln, err := net.ListenTCP("tcp", tcp)
	if err != nil {
		log.Fatalf("[TRACKER] Listen error: %v", err)
		return
	}
	for {
		select {
		case <-tracker.ch:
			ln.Close()
			return
		default:
		}
		ln.SetDeadline(time.Now().Add(time.Second))
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("CONNECTION ACCEPT ERROR: %v", err)
			return
		}
		tracker.Handle(conn)
		conn.Close()
	}
}

func (tracker *Tracker) Stop() {
	close(tracker.ch)
	tracker.wG.Wait()
}

func (tracker *Tracker) Handle(conn net.Conn) {
	body, _ := bufio.NewReader(conn).ReadString('\n')
	body = strings.TrimPrefix(body, "GET ")
	body = strings.TrimSuffix(body, " HTTP/1.1\r\n")

	query, err := url.ParseQuery(body)
	if err != nil {
		log.Fatalf("[TRACKER] url.ParseQuery error: ", err)
		return
	}

	log.Printf("[TRACKER] REQUEST FROM %q: {compact:%q, downloaded:%q, uploaded:%q, left:%q}\n", query.Get("peer_id"), query.Get("compact"), query.Get("downloaded"), query.Get("uploaded"), query.Get("left"))

	peer_id := query.Get("peer_id")
	port, _ := strconv.Atoi(query.Get("port"))
	info_hash := query.Get("info_hash")
	left, _ := strconv.Atoi(query.Get("left"))
	remote_ip := strings.Split(conn.RemoteAddr().String(), ":")[0]

	p := Peer{peer_id, remote_ip, port}
	tracker.AddPeer(info_hash, left, p)

	trackerResponse := TrackerResponse{}
	trackerResponse.Interval = tracker.Interval
	trackerResponse.Peers = tracker.Torrents[info_hash].Peers

	var b bytes.Buffer
	if err := bencode.Marshal(&b, trackerResponse); err != nil {
		log.Fatalf("[TRACKER] BENCODE ERROR: ", err)
		return
	}
	bytes := b.Bytes()

	log.Printf("[TRACKER] RESPONSE: %v ...", string(bytes[:79]))
	conn.Write([]byte("HTTP/1.0 200 OK\r\n"))
	conn.Write([]byte("Content-Type: text/plain\r\n"))
	conn.Write([]byte("Content-Length: " + strconv.Itoa(len(bytes)) + "\r\n"))
	conn.Write([]byte("\r\n"))
	conn.Write(bytes)
}

func (tracker *Tracker) AddPeer(info_hash string, left int, p Peer) {
	tT := tracker.Torrents[info_hash]
	if tT.Peers == nil {
		tT.Peers = []Peer{}
	}

	var found bool
	for i := 0; i < len(tT.Peers); i++ {
		if tT.Peers[i].Id == p.Id {
			found = true
			break
		}
	}
	if !found {
		tT.Peers = append(tT.Peers, p)
	}

	if left > 0 {
		tT.Incomplete++
	} else {
		tT.Complete++
	}

	tracker.Torrents[info_hash] = tT
}
