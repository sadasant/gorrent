package gorrent

import (
	"bitbucket.org/sadasant/gorrent/messages"
	"bytes"
	bencode "code.google.com/p/bencode-go"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	ClientIdLength = 20
	ClientAcronym  = "-GO"
	ClientVersion  = "0001"
	AlphaDigits    = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789"
)

type Client struct {
	Id              string
	TorrentsSession []*TorrentSession
	DownloadPath    string
	Port            int
	ch              chan bool
	wG              *sync.WaitGroup
}

type Peer struct {
	Id   string
	Ip   string
	Port int
}

func NewClient() *Client {
	client := new(Client)
	clientId := ClientAcronym + ClientVersion
	client.Id = clientId + randomString(ClientIdLength-len(clientId))
	client.Port = 6881
	client.DownloadPath = "."
	client.TorrentsSession = []*TorrentSession{}
	client.ch = make(chan bool)
	client.wG = &sync.WaitGroup{}
	return client
}

func (client *Client) AddTorrent(t *Torrent) {
	client.TorrentsSession = append(client.TorrentsSession, t.NewSession(false, len(client.TorrentsSession)))
}

func (client *Client) AddLocalTorrent(t *Torrent) {
	TS := t.NewSession(true, len(client.TorrentsSession))
	client.TorrentsSession = append(client.TorrentsSession, TS)
}

func (client *Client) TrackTorrent(index int) {
	TS := client.TorrentsSession[index]
	torrent := TS.Torrent
	infoHash, _ := torrent.Info.Hash()
	query := url.Values{}

	query.Set("info_hash", infoHash)
	query.Add("peer_id", client.Id)
	query.Add("port", strconv.FormatInt(int64(client.Port), 10))
	query.Add("uploaded", strconv.FormatInt(int64(TS.Uploaded), 10))
	query.Add("downloaded", strconv.FormatInt(int64(TS.Downloaded), 10))
	query.Add("left", strconv.FormatInt(int64(TS.Left), 10))
	query.Add("compact", strconv.FormatInt(1, 10))

	uri := torrent.Announce + "?" + query.Encode()
	log.Printf("[%s] Contacting: %v", client.Id, torrent.Announce)

	resp, err := http.Get(uri)
	defer resp.Body.Close()
	if err != nil {
		log.Fatalf("[%s] Unable to contact the tracker. ERROR: %v", client.Id, err)
		return
	}
	if resp.StatusCode != 200 {
		log.Fatalf("[%s] Unable to contact the tracker. Status code: %v", client.Id, resp.StatusCode)
		return
	}

	err = bencode.Unmarshal(resp.Body, TS.TR)
	if err != nil {
		log.Fatalf("[%s] Bencode Unmarshal ERROR: %v\nBody: %s", client.Id, err, resp.Body)
		return
	}

	log.Printf("[%s] RESPONSE: %v", client.Id, TS.TR)

	// Connect to peers, but proceed with handshake only if TS.Left > 0
	client.ConnectToPeers(index, TS.Left > 0)

	// TODO: Remove this, we only need it to read cleaner logs.
	if len(TS.TR.Peers) > 1 {
		return
	}

	if TS.TR.Interval > 0 {
		time.Sleep(time.Duration(TS.TR.Interval) * time.Second)
		client.TrackTorrent(index)
	}
}

func (client *Client) ConnectToPeers(index int, handshake bool) {
	TS := client.TorrentsSession[index]
	infoHash, _ := TS.Torrent.Info.Hash()
	for i := 0; i < len(TS.TR.Peers); i++ {
		peer := TS.TR.Peers[i]
		if client.Id == peer.Id {
			continue
		}
		if TS.Connects[peer.Id] == nil {
			TS.Connects[peer.Id] = NewConnection(client, peer)
			TS.Connects[peer.Id].Connect()
			TS.Connects[peer.Id].SendHandshake(infoHash)
			// TODO:
			// The bitfield message may only be sent immediately after
			// the handshaking sequence is completed, and before any
			// other messages are sent. It is optional, and need not be
			// sent if a client has no pieces.
			//
			// c.SendBitfield(infoHash)
			TS.Connects[peer.Id].Exchange(TS)
		}
	}
}

func (client *Client) Listen() {
	client.wG.Add(1)
	defer client.wG.Done()
	tcp, err := net.ResolveTCPAddr("tcp", ":"+strconv.Itoa(client.Port))
	if err != nil {
		log.Fatalf("[%s] ResolveTCPAddr error: %v", client.Id, err)
		return
	}
	ln, err := net.ListenTCP("tcp", tcp)
	if err != nil {
		log.Fatalf("[%s] Listen error: %v", client.Id, err)
		return
	}
	for {
		select {
		case <-client.ch:
			ln.Close()
			return
		default:
		}
		log.Printf("[%s] ln.Accept() Loop", client.Id)
		ln.SetDeadline(time.Now().Add(time.Second))
		conn, err := ln.Accept()
		if err != nil {
			log.Println("[%s] Connection accept error: %v", client.Id, err)
		}
		client.Handle(conn)
		conn.Close()
	}
}

func (client *Client) Stop() {
	close(client.ch)
	client.wG.Wait()
}

func (client *Client) Handle(conn net.Conn) {
	client.wG.Add(1)
	defer client.wG.Done()
	log.Printf("[%s] HANDLING CONNECTION WITH %q", client.Id, conn.RemoteAddr())
	var err error

	// HANDSHAKE
	hand, err := client.readHandshake(conn)
	if err != nil {
		log.Printf("[%s] WRONG HANDSHAKE: %s", client.Id, err)
		return
	}
	log.Printf("[%s] OK HANDSHAKE WITH %q", client.Id, hand.PeerId)

	// Get connection
	id := string(hand.PeerId[:20])
	var c *Connection
	var TS *TorrentSession
	var retry int
C:
	for c == nil {
		for i := 0; i < len(client.TorrentsSession); i++ {
			TS = client.TorrentsSession[i]
			if TS.Connects[id] != nil {
				c = TS.Connects[id]
				break C
			}
		}
		if retry > 10 {
			log.Printf("[%s] PEER NOT IN TRACKER: %q", client.Id, id)
			return
		}
		log.Printf("[%s] PEER NOT FOUND: %q, TRACKING AGAIN #%v", client.Id, id, retry)
		retry++
		client.TrackTorrent(TS.Index)
	}

	// BLOCKS
	bytesCount := 0
	buf := make([]byte, TS.Torrent.Info.PieceLength)

	for {
		select {
		case <-client.ch:
			return
		default:
		}
		log.Printf("[%s] READ BLOCKS LOOP", client.Id)
		conn.SetReadDeadline(time.Now().Add(2 * time.Second))

		n, err := conn.Read(buf)
		if err != nil && err != io.EOF {
			if netErr, ok := err.(net.Error); ok && !netErr.Timeout() {
				log.Printf("[%s] Error: %s", client.Id, err)
				return
			}
		}
		if n > len(buf) {
			log.Printf("[%s] Too many bytes for a block: %s", client.Id, n)
			return
		}

		readed := buf[:n]
		bytesCount += n
		c.Data = append(c.Data, readed...)
		log.Printf("[%s] Got %v bytes, len(c.Data): %v", client.Id, n, len(c.Data))

		if len(c.Data) <= 5 {
			continue
		}

		message, n, err := parseData(c.Data, TS.Torrent.Info.PieceLength)
		if err != nil {
			log.Printf("[%s] ERROR: %s", client.Id, err)
			continue
		}
		if n > 0 {
			if n > len(c.Data) {
				log.Printf("[%s] ERROR: %v > %v", client.Id, n, len(c.Data))
				return
			}
			c.Data = c.Data[n:]
			log.Printf("[%s] RECEIVED MESSAGE: {Id: %v, Length: %v}", client.Id, message.Header.Id, message.Header.Length)
			go c.processMessage(message, TS)
		}
	}
}

func (client *Client) readHandshake(conn net.Conn) (hand messages.Handshake, err error) {
	buf := make([]byte, messages.HandshakeLength)
	n, err := conn.Read(buf)
	if err != nil {
		return
	}

	if n != messages.HandshakeLength {
		err = errors.New("Cannot read the handshake")
		return
	}

	err = binary.Read(bytes.NewBuffer(buf), binary.BigEndian, &hand)
	if err != nil {
		return
	}

	return
}

func parseData(data []byte, max_length int) (message messages.Message, n int, err error) {
	n = 0
	var length uint32

	err = binary.Read(bytes.NewBuffer(data[:4]), binary.BigEndian, &length)
	if err != nil {
		return
	}

	int_length := int(length)
	if int_length > (max_length + 9) {
		err = fmt.Errorf("Message length is too long: %v > %v + 9, fisrt 10: % x", length, max_length, data[:10])
		return
	}

	if int_length > len(data) {
		err = fmt.Errorf("I need more data, message length: %v > %v", length, len(data))
		return
	}

	message = messages.Message{}
	message.Header.Length = length

	data = data[4:]
	n += 4
	if int_length > messages.RequestLength {
		int_length -= 4
	}

	if int_length > 0 {
		message.Header.Id = data[0]
		message.Payload = make([]byte, int_length-1)
		data_length := int_length
		if data_length > len(data) {
			data_length = len(data)
		}
		copy(message.Payload, data[1:data_length])
		n += int_length
	}
	return
}

func (client *Client) writeFiles(index int) error {
	TS := client.TorrentsSession[index]
	torrent := TS.Torrent
	info := torrent.Info
	var err error
	if len(info.Files) == 0 {
		// Single file mode
		path := client.DownloadPath + "/" + info.Name
		err = client.writeFile(path, TS.Data)
		if err != nil {
			return err
		}
	} else {
		// Multiple files mode.
		// Create the directory
		dir_path := client.DownloadPath + "/" + info.Name
		err = os.Mkdir(dir_path, 0755)
		if err != nil {
			return err
		}
		var start int
		var end int
		var path string
		for _, file := range info.Files {
			var subdir_path string
			for i := 0; i < len(file.Path)-1; i++ {
				subdir_path += "/" + file.Path[i]
				err = os.Mkdir(dir_path, 0755)
				if err != nil {
					log.Printf("ERROR Mkdir %q: %q", dir_path, err)
					// return err
				}
			}
			path = dir_path + "/" + strings.Join(file.Path, "/")
			end = start + file.Length
			client.writeFile(path, TS.Data[start:end])
			start = end
		}
	}
	return nil
}

func (client *Client) writeFile(path string, data []byte) error {
	log.Printf("WRITING: %q, LEN: %v, DATA: % x", path, len(data), data)
	_, err := os.Stat(path)
	if !os.IsNotExist(err) {
		return err
	}
	if err == nil {
		return fmt.Errorf("File %q exists.", path)
	}
	err = ioutil.WriteFile(path, data, 0755)
	if err != nil {
		return err
	}
	return nil
}
