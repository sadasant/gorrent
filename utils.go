package gorrent

import (
	"math/rand"
	"time"
)

func randomString(size int) string {
	buf := make([]byte, size)

	for i := 0; i < size; i++ {
		buf[i] = AlphaDigits[rand.Intn(len(AlphaDigits))]
	}

	return string(buf)
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}
