package gorrent

import (
	"crypto/sha1"
	"log"
	"math"
	"math/rand"
)

const (
// MaxBlockLength is generally a power of two unless it gets
// truncated by the end of the file.
// All current implementations use 2 15 (32 KB), and close
// connections which request an amount greater than 2 17.
	MaxBlockLength = 1024 * 16
)

// Piece vs Block:
// A piece refers to a portion of the downloaded data that is described
// in the metainfo file, which can be verified by a SHA1 hash.
// A block is a portion of data that a client may request from at least one peer.
// Two or more blocks make up a whole piece, which may then be verified
type Piece struct {
	Data        []byte // This will be filled as blocks come
	Status      []int  // 0: Unattended, 1: Attended: 2: Completed
	Blocks      [][]byte
	BlockLength int
	Hash        string
	Index       int
	Length      int
}

func NewPiece(index, length int, hash string, data []byte) *Piece {
	p := new(Piece)
	data_len := len(data)
	blocks := length / MaxBlockLength
	blocks++
	p.BlockLength = int(math.Ceil(float64(length) / float64(blocks)))
	p.Status = make([]int, blocks)
	p.Blocks = make([][]byte, blocks)
	p.Data = make([]byte, length)
	if len(data) > 0 {
		for i := 0; i < blocks; i += 1 {
			p.Status[i] = 2
			start := i * p.BlockLength
			end := start + p.BlockLength
			if end > data_len {
				end = data_len
			}
			// println(i, start, end)
			if start < end {
				p.Blocks[i] = data[start:end]
			}
		}
	}
	p.Hash = hash
	p.Index = index
	p.Length = length
	return p
}

func (p *Piece) SetBlock(begin int, block []byte) {
	index := begin / p.BlockLength
	if p.Status[index] > 1 {
		log.Printf("Attempt to overwrite data at piece %v, offset %v", p.Index, begin)
		return
	}
	end := begin + len(block)
	copy(p.Data[begin:end], block)
	p.Blocks[index] = block
	p.Status[index] = 2
	p.Length -= p.BlockLength - len(block)
	log.Printf("Added block: {Len: %v, Index: %v, len(data): %v, Begin: %v, End: %v, piece.Length: %v}", len(block), index, len(p.Data), begin, end, p.Length)
}

type BlockRequest struct {
	Begin  int
	Length int
}

func (p *Piece) NextBlock() *BlockRequest {
	indices := p.GetUnattended()
	if len(indices) == 0 {
		return nil
	}

	index := indices[0]
	p.Status[index] = 1
	lastLength := p.Length % p.BlockLength

	blockRequest := new(BlockRequest)
	blockRequest.Begin = index * p.BlockLength

	if index == len(p.GetCompleted())-1 && lastLength != 0 {
		blockRequest.Length = lastLength
	} else {
		blockRequest.Length = p.BlockLength
	}

	return blockRequest
	return nil
}

func (p *Piece) IsValid() bool {
	hash := sha1.New()
	hash.Write(p.Data)
	return string(hash.Sum(nil)) == p.Hash
}

func (p *Piece) IsComplete() bool {
	len_status := len(p.Status)
	if len_status == 0 {
		return false
	}
	var count int
	for i := 0; i < len_status; i++ {
		if p.Status[i] == 2 {
			count++
		}
	}
	return count == len_status
}

func (p *Piece) IsActive() bool {
	for i := 0; i < len(p.Status); i++ {
		if p.Status[i] > 0 {
			return true
		}
	}
	return false
}

func (p *Piece) GetUnattended() (indices []int) {
	for i := 0; i < len(p.Status); i++ {
		if p.Status[i] == 0 {
			indices = append(indices, i)
		}
	}
	return
}

func (p *Piece) GetCompleted() (indices []int) {
	for i := 0; i < len(p.Status); i++ {
		if p.Status[i] == 2 {
			indices = append(indices, i)
		}
	}
	return
}

func (p *Piece) ValidHash() bool {
	sha1_hash := sha1.New()
	sha1_hash.Write(p.Data)
	hash := string(sha1_hash.Sum(nil))
	log.Printf("HASH: % x", hash)
	log.Printf("HASH: % x", p.Hash)
	return hash == p.Hash
}

func (p *Piece) Reset() {
	blocks := len(p.Blocks)
	p.Status = make([]int, blocks)
	p.Blocks = make([][]byte, blocks)
	p.Data = make([]byte, len(p.Data))
}

func randomPiece(pieces []*Piece) *Piece {
	randomIndex := rand.Intn(len(pieces))
	return pieces[randomIndex]
}
